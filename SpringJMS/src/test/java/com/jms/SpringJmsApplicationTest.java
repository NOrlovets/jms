package com.jms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.TimeUnit;

import com.jms.Consumer.Receiver;
import com.jms.Sender.Sender;
import org.apache.activemq.junit.EmbeddedActiveMQBroker;
import org.junit.AfterClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringJmsApplicationTest {

    private static ApplicationContext applicationContext;

    @Autowired
    void setContext(ApplicationContext applicationContext) {
        SpringJmsApplicationTest.applicationContext = applicationContext;
    }

    @AfterClass
    public static void afterClass() {
        ((ConfigurableApplicationContext) applicationContext).close();
    }

    @ClassRule
    public static EmbeddedActiveMQBroker broker = new EmbeddedActiveMQBroker();

    @Autowired
    private Sender sender;

    @Autowired
    private Receiver receiver;

    @Test
    public void testReceive() throws Exception {
        sender.send("helloworld.q", "Hello Spring JMS ActiveMQ!");
        sender.send("helloworld.q", "1");
        sender.send("helloworld.q", "2");
        sender.send("helloworld.q", "3");
        sender.send("helloworld.q", "4");
        sender.send("helloworld.q", "5");
        sender.send("helloworld.q", "6");
        sender.send("helloworld.q", "7");
        sender.send("helloworld.q", "8");
        sender.send("helloworld.q", "9");
        sender.send("helloworld.q", "10");
        sender.send("helloworld.q", "11");
        sender.send("helloworld.q", "12");
        receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
        sender.send("helloworld.q", "13");
        sender.send("helloworld.q", "14");
        sender.send("helloworld.q", "15");
        assertThat(receiver.getLatch().getCount()).isEqualTo(0);
    }
}